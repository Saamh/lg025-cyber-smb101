# Docker - Introduction - Images

# Objectif

## Analyser une image

[GitHub - wagoodman/dive: A tool for exploring each layer in a docker image](https://github.com/wagoodman/dive#installation)

## Build une image docker

[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

[docker build](https://docs.docker.com/engine/reference/commandline/build/)

```jsx
# Projet du tutoriel Docker

git clone https://github.com/docker/getting-started.git
```

```jsx
# syntax=docker/dockerfile:1
FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

```bash
# Build une image Docker

docker build .

# Build et nommer une image même temps

docker build -t nomimage:tag .
```

[Sample application](https://docs.docker.com/get-started/02_our_app/)

[https://github.com/docker/getting-started](https://github.com/docker/getting-started)

[Docker Hub Container Image Library | App Containerization](https://hub.docker.com/)

# Documentation

[Orientation and setup](https://docs.docker.com/get-started/)

[SIGNAL](http://manpagesfr.free.fr/man/man7/signal.7.html)

[Docker run reference](https://docs.docker.com/engine/reference/run/)

https://github.com/spaceship-prompt/spaceship-prompt
