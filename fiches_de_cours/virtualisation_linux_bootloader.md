# Virtualisation - VM Linux - Bootloader - Disque Virtuel



## TP VM Linux Bootloader Montage Disque

Créer une VM Linux

Accéder au bootloader GRUB

Créer un disque virtuel

Création de partition, système de fichier et montage/démontage de volume

Commandes utiles:

```
mount
umount
mkfs.ext4
ls
man 5 /etc/fstab
man 1 mount
```

## Ressources

[Hiérarchie du système de fichiers (filesystem) - #Linux 1.4](https://xavki.blog/tutoriels-et-formation-linux-fr/#ixch0N_Ty6c)

[Le MAN : débuter avec la documentation & rtfm - #Linux 1.5](https://xavki.blog/tutoriels-et-formation-linux-fr/#4s0SEUdyrAo)

[Stockage : ajouter un disque, le formater et le monter/démonter (VirtualBox) - #Linux 1.19](https://xavki.blog/tutoriels-et-formation-linux-fr/#pQKPpoonuSI)

[Mount & Fstab : options, bind, fail sur reboot... - #Linux 1.21](https://xavki.blog/tutoriels-et-formation-linux-fr/#52gJ9hJjjtM)

[An Introduction to Linux Basics | DigitalOcean](https://www.digitalocean.com/community/tutorials/an-introduction-to-linux-basics)

[virtualbox [Wiki ubuntu-fr]](https://doc.ubuntu-fr.org/virtualbox)

[Linux Jargon Buster: What is Grub in Linux? What is it Used for?](https://itsfoss.com/what-is-grub/)

[grub-pc [Wiki ubuntu-fr]](https://doc.ubuntu-fr.org/grub-pc)

[montage [Wiki ubuntu-fr]](https://doc.ubuntu-fr.org/montage)