# 27/06/2022 - Kubernetes - Cluster K3S - Workload

[Apprendre les bases de Kubernetes](https://kubernetes.io/fr/docs/tutorials/kubernetes-basics/)

[Lightweight Kubernetes](https://k3s.io/)

[Setting up a Kubernetes cluster using Raspberry Pi's (K3s) and Portainer](https://theselfhostingblog.com/posts/setting-up-a-kubernetes-cluster-using-raspberry-pis-k3s-and-portainer/)

[Git - What is Git?](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)