# 20/04/2022 - Docker - Introduction - Containers

# Objectif du cours

- Lancer un container
- Lancer un shell dans un container en mode interactif
- Lancer un container avec volume
    - créer un fichier dans le volume
    - stopper le container
    - créer un nouveau container et attacher le volume précédemment créé
- Lancer plusieurs containers et les faire communiquer entre eux
    - nginx (serveur web)
    - ubuntu (avec utilitaire curl pour faire des requêtes sur le serveur nginx)

# Travaux Pratiques

## Pré-requis

```bash
# Tester si docker fonctionne
docker run hello-world
```

## Lancer un container

Utiliser une image simple, par exemple hello-world

[docker run](https://docs.docker.com/engine/reference/commandline/run/)

## Lancer un shell interactif dans un container

```bash
# docker run [OPTIONS] IMAGE [COMMAND]

docker run -it ubuntu /bin/bash 
```

## Lancer un container avec volume

```bash

# Créer un volume et l'attacher a un nouveau container
docker volume create nom_volume

docker run -v nom_volume:/mnt/volume -it ubuntu /bin/bash

	# Lister les montages dans le container
	$ mount

docker volume ls
docker volume inspect nom_volume

# Monter un dossier local dans un container

cd /tmp
mkdir montage
cd montage
echo "test" > test.txt

	# Monter le dossier montage dans mon container

docker run -v /tmp/montage:/mnt/volume -it ubuntu /bin/bash

	# Monter le dossier en read-only (ajouter :ro)
docker run -v /tmp/montage:/mnt/volume:ro -it ubuntu /bin/bash

mkdir $HOME/montage

docker run -v $HOME/montage:/mnt/volume -it ubuntu /bin/bash

	$ touch /mnt/volume/test-fichier.txt
	$ ls -lh /mnt/volume/test-fichier.txt
	$ exit

ls -lh $HOME/montage/test-fichier.txt
```

## Containers en réseau

```bash
# Lister les réseaux disponibles
docker network ls

        marasi_antoine@smb101-lg025:~/tmp$ docker network ls
        NETWORK ID     NAME      DRIVER    SCOPE
        702b7f5633a9   bridge    bridge    local
        92e3cb4ecd00   host      host      local
        83f2ffc9f4f8   none      null      local

# Créer un réseau bridge à votre nom
docker network create $USER

# Voir les paramètres de votre réseau
docker network inspect $USER

# Créer un container et l'attacher à votre réseau
docker run -d --network $USER -it ubuntu sleep infinity

# Voir si votre container est bien connecté au réseau
docker container ls
docker container inspect nom_de_votre_container

# Lancer un shell dans un container qui tourne en arrière plan
docker exec -it nom_du_container /bin/bash

# Lancer un serveur web en arrière plan sur le réseau bridge
docker run -d --network $USER -p 8080:80 nginx

# A coté, lancer un autre container ubuntu en mode interactif et effectuer une requete http à l'aide de curl
docker run --network $USER  -it ubuntu /bin/bash
    
    # Installer curl pour effectuer des requetes http
    $ apt update
    $ apt install -y curl
    $ curl http://<ip_container_nginx>

Depuis votre host, ouvrez un navigateur et ouvrez la page web http://localhost:8080
```

## Voir les traces des container

```bash
# Lancer un container en arrière plan avec --detached
docker run --name random_log --detached -it wernight/funbox nyancat

# Voir les logs du container
docker logs random_log

# Suivre en direct la sortie du container
docker logs random_log -f
```

# Ressources

[docker exec](https://docs.docker.com/engine/reference/commandline/exec/)

![Untitled](20%2004%202022%20-%20Docker%20-%20Introduction%20-%20Containers%2044b8bb4afd7240d5b9d542cabba9fdc3/Untitled.png)

[Container Management | Kubernetes GUI | Docker Swarm GUI | Portainer](https://www.portainer.io/)