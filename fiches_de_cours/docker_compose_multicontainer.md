# Docker - Docker Compose - Multicontainers

# Objectif du cours

- Finir TP du cours précédent

- Refaire la même chose mais cette fois-ci dans Docker Compose

# Docker Compose

[Overview of Docker Compose](https://docs.docker.com/compose/)

## Exemple docker-compose.yml

```yaml
services:
  nginx:
    image: nginx
    ports:
      - 8080:80
    volumes:
      - db-data:/mnt/webserver
  database:
    image: postgres
    environment:
      - POSTGRES_PASSWORD=password
    volumes:
      - db-data:/mnt/database
  redis:
    image: redis

volumes:
  db-data:
```

## Installer le plugin docker compose

Si `docker compose` ne fonctionne pas, il faut installer le plugin: 

```
sudo apt-get update
sudo apt-get install docker-compose-plugin
```

## Commandes docker compose

```yaml
# Lancer la stack docker compose 
docker compose up

# Lancer la stack en mode détaché 
docker compose up -d

# Voir l'état des container de la stack docker compose
docker compose ls
docker compose ps
docker compose top

# Stopper la stack
docker compose down

# Executer un programme dans un container; i.e lancer un shell
docker compose exec nomducontainer /bin/bash

# Lancer une stack docker compose en arrière plan tout en forçant le build
docker compose up --build -d
```

# Travaux Pratiques

Plusieurs container, au moins une custom 

- Dockerfile
- build

Au moins deux de ces containers partagent un volume/fichier

Au moins deux de ces container communiquent via un même réseau bridge autre que le défaut

Exposer un/plusieurs port de vos container sur la machine hote

1 Container ubuntu

1 container nginx

1 container mysql

 

Container ubuntu qui peut curl sur le container nginx

Container ubuntu qui a un client mysql installé et peut interagir avec le container mysql


# Ressources

[Compose specification](https://docs.docker.com/compose/compose-file/)

[The Official YAML Web Site](https://yaml.org/)

[Use the Docker command line](https://docs.docker.com/engine/reference/commandline/cli/)

[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

[Docker Hub](https://hub.docker.com/search?q=)

[CNCF Cloud Native Interactive Landscape](https://landscape.cncf.io/)