# Virtualisation - Réseau - Interconnectivité

# Objectif

Avoir 3 machines virtuelles:

- 1 qui ne peut communiquer qu’avec la machine hote
- 1 qui ne peut communiquer qu’avec une autre machine virtuelle (mais pas l’hote)
- 1 qui peut communiquer avec une autre machine virtuelle et aussi sortir sur Internet

Ensuite, connecter deux machines avec une interface supplémentaire sur le même réseau local, en plus de l'interface initiale.

# Ressources

[Virtualisation - Les types de connexion au réseau | Administration Réseau | IT-Connect](https://www.it-connect.fr/virtualisation-les-types-de-connexion-au-reseau)

[Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

[Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

![IMG_20220321_115404.jpg](virtualisation_reseau_interconnection/IMG_20220321_115404.jpg)

```jsx
docker run hello-world
```
