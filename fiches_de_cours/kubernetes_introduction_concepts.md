# 01/06/2022 - Kubernetes - Introduction - Concepts

# Objectif du cours

- Introduction à Kubernetes
- TP Kubernetes

# Introduction a Kubernetes

[Getting Started With Kubernetes and Container Orchestration](https://qconuk2019.container.training/#76)

# TP Kubernetes

[Install the gcloud CLI | Google Cloud](https://cloud.google.com/sdk/docs/install#deb)

[Installer et configurer kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)

[aerobic-star-344312-8d68667312b0.json](01%2006%202022%20-%20Kubernetes%20-%20Introduction%20-%20Concepts%202c714ce1a8674d53b9d1b2b30cfd787a/aerobic-star-344312-8d68667312b0.json)

```yaml
			# gcloud auth activate-service-account --key-file=aerobic-star-344312-8d68667312b0.json

gcloud auth activate-service-account --key-file="$HOME/Downloads/aerobic-star-344312-8d68667312b0.json"
	Activated service account credentials for: [lg025-193@aerobic-star-344312.iam.gserviceaccount.com]

gcloud container clusters get-credentials lg025 --zone europe-west1-b --project aerobic-star-344312
	Fetching cluster endpoint and auth data.
	kubeconfig entry generated for lg025.

kubectl version
	Client Version: version.Info{Major:"1", Minor:"24", GitVersion:"v1.24.1", GitCommit:"3ddd0f45aa91e2f30c70734b175631bec5b5825a", GitTreeState:"clean", BuildDate:"2022-05-24T12:26:19Z", GoVersion:"go1.18.2", Compiler:"gc", Platform:"linux/amd64"}
	Kustomize Version: v4.5.4
	Server Version: version.Info{Major:"1", Minor:"21", GitVersion:"v1.21.11-gke.1100", GitCommit:"20da4c21b3a6b1a56ff6ad5ecb7dee013aaf1b83", GitTreeState:"clean", BuildDate:"2022-04-01T09:40:07Z", GoVersion:"go1.16.15b7", Compiler:"gc", Platform:"linux/amd64"}
```

[Getting Started With Kubernetes and Container Orchestration](https://qconuk2019.container.training/#127)

[Getting Started With Kubernetes and Container Orchestration](https://qconuk2019.container.training/#158)

```yaml
kubectl get nodes
kubectl get namespaces
kubectl get deployments --namespace kube-system
```

# Resources

[Container Training](https://container.training/)

[Getting Started With Kubernetes and Container Orchestration](https://qconuk2019.container.training/)