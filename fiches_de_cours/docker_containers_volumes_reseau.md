# Docker - Containers - Volumes - Réseau

# Objectif du cours

- Revoir les notions des sessions précédentes

- Lancer plusieurs containers à partir d’images custom qui peuvent communiquer entre eux et se partager des fichiers

# Travaux Pratiques

Plusieurs container, au moins une image custom 

- Dockerfile
- build

Au moins deux de ces containers partagent un volume/fichier

Au moins deux de ces container communiquent via un même réseau bridge autre que le défaut

Exposer un/plusieurs port de vos container sur la machine hote

1 Container ubuntu

1 container nginx

1 container mysql
 

Container ubuntu qui peut curl sur le container nginx

Container ubuntu qui a un client mysql installé et peut interagir avec le container mysql


# Ressources

[Use the Docker command line](https://docs.docker.com/engine/reference/commandline/cli/)

[Dockerfile reference](https://docs.docker.com/engine/reference/builder/)

[Docker Hub](https://hub.docker.com/search?q=)

[CNCF Cloud Native Interactive Landscape](https://landscape.cncf.io/)