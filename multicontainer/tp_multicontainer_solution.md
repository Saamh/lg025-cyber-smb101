# Lancement de plusieurs containers avec Docker Compose

Voici un exemple de fichier docker-compose.yml qui permet de lancer plusieurs containers et de répondre aux critères demandés :

```yaml
version: '3'

services:
  ubuntu:
    image: ubuntu
    volumes:
      - my_volume:/data
    networks:
      - my_network
    command: bash -c "sleep infinity"

  nginx:
    image: nginx
    volumes:
      - my_volume:/usr/share/nginx/html
    ports:
      - "8080:80"
    networks:
      - my_network

  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: example
    networks:
      - my_network

networks:
  my_network:

volumes:
  my_volume:
```

## Explication

Nous avons trois services : ubuntu, nginx et mysql.

- Le service ubuntu utilise l'image ubuntu, partage le volume my_volume avec le service nginx, expose le port 8080 sur la machine hôte, utilise le réseau my_network et exécute la commande sleep infinity pour maintenir le container en vie.

- Le service nginx utilise l'image nginx, partage le volume my_volume avec le service ubuntu, expose le port 80 sur le réseau my_network.

- Le service mysql utilise l'image mysql, déclare le mot de passe root MYSQL_ROOT_PASSWORD et utilise le réseau my_network.

- Nous avons également un réseau nommé my_network qui permet à ubuntu, nginx et mysql de communiquer entre eux.

- Enfin, nous avons un volume nommé my_volume qui permet aux services ubuntu et nginx de partager un fichier.

## Test de la configuration

Pour tester la configuration, nous pouvons exécuter les commandes suivantes :

Pour lancer les containers :

`docker compose up -d`

Pour vérifier que les containers sont en cours d'exécution :

`docker compose ps`

Pour vérifier que le container ubuntu peut lancer une requête curl sur le container nginx :

```
# Installer curl dans l'image Ubuntu
docker compose exec ubuntu bash -c 'apt -y update && apt -y install curl'

# Créer un fichier html pour le serveur nginx
docker compose exec ubuntu bash -c 'echo Hello > /data/index.html'

# Faire une requête web sur le container nginx pour afficher le fichier index.html
docker compose exec ubuntu curl nginx
```

Pour vérifier que le container ubuntu peut interagir avec le container mysql :

```
# Installer le client mysql dans le container Ubuntu
docker compose exec ubuntu bash -c 'apt -y update && apt -y install mysql-client'

# Se connecter à la base MySQL depuis le container Ubuntu
docker compose exec ubuntu mysql -uroot -pexample -h mysql
```

## Conclusion

Nous avons créé un fichier docker-compose.yml qui permet de lancer plusieurs containers répondant aux critères demandés. Nous avons également testé la configuration en vérifiant que les containers peuvent communiquer entre eux.