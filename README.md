# Liste des pages des sessions de cours

[Virtualisation - VM Linux - Bootloader - Disque Virtuel](fiches_de_cours/virtualisation_linux_bootloader.md)

[Virtualisation - Réseau - Interconnectivité](fiches_de_cours/virtualisation_reseau_interconnection.md)

[Docker - Introduction - Images](fiches_de_cours/docker_introduction_images.md)

[Docker - Introduction - Containers](fiches_de_cours/docker_introduction_containers.md)

[Docker - Containers - Volumes - Réseau](fiches_de_cours/docker_containers_volumes_reseau.md)

[Docker - Docker Compose - Multicontainers](fiches_de_cours/docker_compose_multicontainer.md)

[Kubernetes - Introduction - Concepts](fiches_de_cours/kubernetes_introduction_concepts.md)

[Kubernetes - Cluster K3S - Workload](fiches_de_cours/kubernetes_cluster_k3s.md)

# Ressources **en français**

Excellentes vidéos **en français** sur beaucoups de sujets dont ceux abordés durant ce module mais pas seulement 🙂

[Parcours de formation Devops - Xavki](https://xavki.blog/)

## OS - Linux

[Tutoriels et Formation Linux Fr - Xavki](https://xavki.blog/tutoriels-et-formation-linux-fr/)

[https://www.youtube.com/watch?v=AcZ87MTiXr4&list=PLP0aqyZ5GFdm4xat5-347L3ZzqkjZG4bk&index=3](https://www.youtube.com/watch?v=AcZ87MTiXr4&list=PLP0aqyZ5GFdm4xat5-347L3ZzqkjZG4bk&index=3)

## Virtualisation

[https://www.youtube.com/watch?v=4J_00mQ5BAs&list=PLP0aqyZ5GFdm4xat5-347L3ZzqkjZG4bk&index=4](https://www.youtube.com/watch?v=4J_00mQ5BAs&list=PLP0aqyZ5GFdm4xat5-347L3ZzqkjZG4bk&index=4)

## Docker

[Docker - tutoriaux français - Xavki](https://xavki.blog/docker-tutoriaux-francais/)

[https://www.youtube.com/watch?v=caXHwYC3tq8](https://www.youtube.com/watch?v=caXHwYC3tq8)

## Docker Compose

[Docker compose - tutoriaux français - Xavki](https://xavki.blog/docker-compose-tutoriaux-francais/)

## Kubernetes

[Kubernetes - tutoriaux français - Xavki](https://xavki.blog/kubernetes-tutoriaux-francais/)

[https://www.youtube.com/watch?v=NChhdOZV4sY](https://www.youtube.com/watch?v=NChhdOZV4sY)

# Ressources en Anglais

## Docker

[Introduction to Containers](https://container.training/intro-selfpaced.yml.html#1)
